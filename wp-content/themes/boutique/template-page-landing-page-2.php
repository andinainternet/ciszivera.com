
<?php /**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: CISZI Landing 2
 *
 * @package ciszi-theme
 */
get_header(); ?>
<div class="header-content-main">
  <div class="header-content-txt">
    <h3><?php the_field(landing_txt_header) ;?></h3>
  </div>
  <div class="header-content-vid">
    <?php the_field(landing_vid_header); ?>
  </div>
  <div class="header-content-txt-2">
    <h3><?php the_field(landing_txt_header_2) ;?></h3>
  </div>
</div>
<div id="primary" class="conten-area">
  <main id="main" role="main" class="site-main">
    <div class="landing-page-main landing-page-2">
      <div style="background-color: <?php the_field(l2_bg_color1);?> " class="landing-page-section1 landing-page-row row">
        <div class="landing-page-section1-container">
          <div class="container-image-section1 col-lg-4 col-md-4 hidden-sm hidden-xs"><img src="<?php the_field(l2_img_1);?>" alt=""/></div>
          <div class="container-text-section1 col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <?php the_field(l2_txt_content); ?>
          </div>
          <div class="container-image-section1 col-lg-4 col-md-4 col-sm-12 hidden-xs"><img src="<?php the_field(l2_img_2);?>" alt=""/></div>
        </div>
      </div>
      <div class="landing-page-section2 landing-page-row row">
        <div class="landing-page-col col-lg-5 col-md-5 col-sm-12 col-xs-12">
          <div class="landing-page-col-excerpt">
            <?php echo do_shortcode(the_field(landing_page_2_ciszi_text_2)); ?>
          </div>
        </div>
        <div class="landing-page-col col-lg-7 col-md-7 col-sm-12 col-xs-12">
          <div class="landing-page-col-excerpt-2">
            <?php echo do_shortcode(the_field(landing_page_2_ciszi_text_3)); ?>
          </div>
          <div class="landing-page-2-col-icos">
            <div class="col-ico-2 col-ico-pdf"><i class="fa fa-file-pdf-o"></i><a href="<?php the_field(link_pdf_2); ?>">
                <h5><?php the_field(link_pdf_txt);?></h5></a></div>
            <div class="col-ico-1 col-ico-audio"><i class="fa fa-volume-up"></i><a href="<?php the_field(link_audio_2); ?>">
                <h5>Escuchalo</h5></a></div>
          </div>
        </div>
      </div>
      <div style="background-color: <?php the_field(l2_bg_color2);?> " class="landing-page-section3 landing-page-row row">
        <div class="landing-page-subrow col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="landing-page-col-excerpt">
            <?php echo do_shortcode(the_field(landing_page_2_ciszi_text_4)); ?>
          </div>
        </div>
        <div class="landing-page-subrow col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="landing-page-col-address">
            <div class="address-1 adress-item col-lg-4 col-md-4 col-sm-4 col-xs-12"><i class="fa fa-map-marker"></i>
              <h4><?php the_field(direccion_evento_1); ?></h4><a href="<?php the_field(destino_evento_1); ?>">Comprar Entrada</a>
            </div>
            <div class="address-2 adress-item col-lg-4 col-md-4 col-sm-4 col-xs-12"><i class="fa fa-map-marker"></i>
              <h4><?php the_field(direccion_evento_2); ?></h4><a href="<?php the_field(destino_evento_2); ?>">Comprar Entrada					</a>
            </div>
            <div class="address-3 adress-item col-lg-4 col-md-4 col-sm-4 col-xs-12"><i class="fa fa-map-marker"></i>
              <h4><?php the_field(direccion_evento_3); ?></h4><a href="<?php the_field(destino_evento_3); ?>">Comprar Entrada</a>
            </div>
          </div>
        </div>
      </div>
      <div id="registro" class="landing-page-ciszi-full-form row">
        <div class="landing-page-col col-lg-6 col-md-6 col-sm-6 col-xs-12 l3-form-imgpack">
          <div class="landing-page-item landing-page-image-form"><img src="<?php the_field(full_form_img); ?>" alt=""/></div>
        </div>
        <div class="landing-page-col col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="landing-page-item">
            <h3>Registrate ahora, es gratis</h3>
            <p>Deja tu email para conseguir mas información</p>
            <div class="l3-form-main">
              <?php echo do_shortcode(the_field(full_form_form)); ?>
            </div>
          </div>
          <div class="landing-page-item-social-media">
            <h2>Compartelo con quienes quieras</h2>
            <?php echo do_shortcode('[wpusb]'); ?>
          </div>
        </div>
      </div>
      
      
    </div>
  </main>
</div>
<?php get_footer(); ?>