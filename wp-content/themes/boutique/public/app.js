(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

jQuery(document).ready(function () {

	var url = window.location.host;
	var brandUrl = '/wp-content/themes/ciszi-theme/assets/images/logo_verde.svg';
	var brandLogo = brandUrl;

	var siteSearch = $('.site-search');
	var searchPlaceHolder = $('#woocommerce-product-search-field').attr('placeholder');
	var searchDisable = 'placeholder-search-field-disable';
	var searchActive = 'placeholder-search-field-active';

	var separator = '<img href="../../ciszi-theme/assets/images/menu-dots.png"/>';

	$('.site-search').hover(function () {
		$(this).toggleClass('activebox');
	});

	$('#woocommerce-product-search-field').attr('placeholder', 'Buscador');
	$('#woocommerce-product-search-field').addClass(searchDisable);

	$('#woocommerce-product-search-field').hover(function () {
		$('#woocommerce-product-search-field').toggleClass(searchDisable);
		$('#woocommerce-product-search-field').toggleClass(searchActive);
	});

	$('#menu-primary >li > a').append(separator);

	$('#footer_brandLogo').attr('src', brandLogo);

	// HOME : FUNCTION ABOUT ANIMATION SERVICES

	$('.serv-item-layout-1').hover(function () {
		$('.serv-item-layout-1').toggleClass('serv-layout-on');
		$('.serv-item-layout-1').toggleClass('serv-layout-off');
		$('.serv-item-txt-1').toggleClass('serv-item-txt-up');
		$('.serv-item-txt-1').toggleClass('serv-item-txt-down');
	});
	$('.serv-item-layout-2').hover(function () {
		$('.serv-item-layout-2').toggleClass('serv-layout-on');
		$('.serv-item-layout-2').toggleClass('serv-layout-off');
		$('.serv-item-txt-2').toggleClass('serv-item-txt-up');
		$('.serv-item-txt-2').toggleClass('serv-item-txt-down');
	});
	$('.serv-item-layout-3').hover(function () {
		$('.serv-item-layout-3').toggleClass('serv-layout-on');
		$('.serv-item-layout-3').toggleClass('serv-layout-off');
		$('.serv-item-txt-3').toggleClass('serv-item-txt-up');
		$('.serv-item-txt-3').toggleClass('serv-item-txt-down');
	});
	$('.serv-item-layout-4').hover(function () {
		$('.serv-item-layout-4').toggleClass('serv-layout-on');
		$('.serv-item-layout-4').toggleClass('serv-layout-off');
		$('.serv-item-txt-4').toggleClass('serv-item-txt-down');
	});

	// LIBRERIA : FUNCTION ABOUT ANIMATION LIBRARY

	$('.lib-item-txt-main').hover(function () {
		$(this).toggleClass('item-txt-main-show');
		$(this).toggleClass('item-txt-main-hide');
	});

	$('#item_lib2').hover(function () {
		$('#item_lib2').toggleClass('item-txt-main-show');
		$('#item_lib2').toggleClass('item-txt-main-hide');
	});

	$('#item_lib3').hover(function () {
		$('#item_lib3').toggleClass('item-txt-main-show');
		$('#item_lib3').toggleClass('item-txt-main-hide');
	});

	$('#item_lib4').hover(function () {
		$('#item_lib4').toggleClass('item-txt-main-show');
		$('#item_lib4').toggleClass('item-txt-main-hide');
	});

	$('#item_lib5').hover(function () {
		$('#item_lib5').toggleClass('item-txt-main-show');
		$('#item_lib5').toggleClass('item-txt-main-hide');
	});

	$('input[type=radio]').attr('checked', 'checked');

	// horizontal scroll					
	// $("#makeMeScrollable").smoothDivScroll({
	// 	mousewheelScrolling: "allDirections",
	// 	manualContinuousScrolling: true,
	// 	autoScrollingMode: "onStart"
	// });	

	// if($("#contenedorajax").html() == ""){ 
	// 	$("#contenedorajax").addClass('ajaxHeight');
	// 	//alert("No existen elementos"); 
	// }  

	$(".usearchbtn").click(function () {
		console.log('funciona');
		$("#contenedorajax").removeClass('ajaxHeight');
	});

	$('.bar-postFormat-audio').addClass('fa', 'fa-volume-down');
	$('.bar-postFormat-video').addClass('fa', 'fa-video-camera');

	var btnAjax = $('.usearchbtn');
	var btnAjax_class_on = 'usearchbtn-on';

	var btnAjax_observer = 'btnAjax-observer';
	var btnAjax_estilo = 'usfbtn-activeBar';

	var defaultActive = $('#uwpqsffrom_185 > #uwpqsf_btn > .usearchbtn');

	$(defaultActive).addClass('usfbtn-activeBar');

	$(btnAjax).addClass(btnAjax_observer);

	$(btnAjax).click(function (e) {
		$(btnAjax).removeClass(btnAjax_estilo);

		$(this).addClass(btnAjax_estilo);
	});

	//REMOVE CLASS FA > DEFAULT BOX POST
	$('.bar-formatPost').removeClass('fa');

	//ADD MENU MY LIBRARY

	//<li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders">
	// <a href="http://localhost/test.tupino.com/ciszi/mi-cuenta/orders/">Pedidos</a></li>

	var dshMenu = $('.woocommerce-MyAccount-navigation-link--orders');
	var dshAdm = 'Hello';
	var dshLi = '<li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--my-folders"><a id="cisziFolders" href="#">Mi Libreria</a></li>';
	var cisziFolders = '../mi-libreria';
	// var crs_menu_itm 	= '<li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--my-courses"><a id="crs_menu_itm" href="#">Mis Cursos</a></li>';
	// var crs_link		= 'http://localhost/test.tupino.com/kaybpm-mat/mis-cursos/';

	// $('.woocommerce-MyAccount-navigation ul').prepend(crs_menu_itm);
	// $('#crs_menu_itm').attr('href', "./../mis-cursos");

	$('.woocommerce-MyAccount-navigation-link--orders').prepend(dshLi);
	$('#cisziFolders').attr('href', "../mi-libreria");

	//CUSTOM FOOTER MENU

	$('.search').click(function () {
		window.location = "./mi-libreria";
	});

	//CUSTOM HEADER POST

	var head_post_div = '<div id="head_post_div"></div>';
	var posted_on = $('.posted-on');
	var post_author = $('.author');

	$('.entry-header').append(head_post_div);
	$('#head_post_div').append(posted_on, post_author);

	//CUSTOM CARD POSTS

	var card_txt = $('.card-item-txt');
	var card_on = 'card-item-txt-on';
	var card_off = 'card-item-txt-off';

	$(card_txt).hover(function () {
		$(this).toggleClass(card_on);
		$(this).toggleClass(card_off);
	});

	//POST

	var author_link = $('.author > a');
	var cat_link = $('.cat-links > a');

	$(author_link).attr('href', '#');
	$(cat_link).attr('href', '#');
});

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmNcXGxpYlxcanNcXGluZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7QUNBQSxPQUFBLEFBQU8sVUFBUCxBQUFpQixNQUFNLFlBQVcsQUFFakM7O0tBQUksTUFBUyxPQUFBLEFBQU8sU0FBcEIsQUFBNkIsQUFDN0I7S0FBSSxXQUFKLEFBQWdCLEFBQ2hCO0tBQUksWUFBSixBQUFrQixBQUVsQjs7S0FBSSxhQUFjLEVBQWxCLEFBQWtCLEFBQUUsQUFDcEI7S0FBSSxvQkFBb0IsRUFBQSxBQUFFLHFDQUFGLEFBQXVDLEtBQS9ELEFBQXdCLEFBQTRDLEFBQ3BFO0tBQUksZ0JBQUosQUFBb0IsQUFDcEI7S0FBSSxlQUFKLEFBQW1CLEFBRW5COztLQUFJLFlBQUosQUFBa0IsQUFFbEI7O0dBQUEsQUFBRSxnQkFBRixBQUFrQixNQUFNLFlBQVUsQUFDakM7SUFBQSxBQUFFLE1BQUYsQUFBUSxZQURULEFBQ0MsQUFBb0IsQUFDcEIsQUFFRDs7O0dBQUEsQUFBRSxxQ0FBRixBQUF1QyxLQUF2QyxBQUE0QyxlQUE1QyxBQUEyRCxBQUMzRDtHQUFBLEFBQUUscUNBQUYsQUFBdUMsU0FBdkMsQUFBZ0QsQUFFaEQ7O0dBQUEsQUFBRSxxQ0FBRixBQUF1QyxNQUFNLFlBQVUsQUFDdEQ7SUFBQSxBQUFFLHFDQUFGLEFBQXVDLFlBQXZDLEFBQW1ELEFBQ25EO0lBQUEsQUFBRSxxQ0FBRixBQUF1QyxZQUZ4QyxBQUVDLEFBQW1ELEFBQ25ELEFBRUQ7OztHQUFBLEFBQUUseUJBQUYsQUFBMkIsT0FBM0IsQUFBa0MsQUFFbEM7O0dBQUEsQUFBRSxxQkFBRixBQUF1QixLQUF2QixBQUE0QixPQUE1QixBQUFtQyxBQUVuQyxBQUVBOzs7O0dBQUEsQUFBRSx1QkFBRixBQUF5QixNQUFNLFlBQVcsQUFDekM7SUFBQSxBQUFFLHVCQUFGLEFBQXlCLFlBQXpCLEFBQXFDLEFBQ3JDO0lBQUEsQUFBRSx1QkFBRixBQUF5QixZQUF6QixBQUFxQyxBQUNyQztJQUFBLEFBQUUsb0JBQUYsQUFBc0IsWUFBdEIsQUFBa0MsQUFDbEM7SUFBQSxBQUFFLG9CQUFGLEFBQXNCLFlBSnZCLEFBSUMsQUFBa0MsQUFDbEMsQUFDRDs7R0FBQSxBQUFFLHVCQUFGLEFBQXlCLE1BQU0sWUFBVyxBQUN6QztJQUFBLEFBQUUsdUJBQUYsQUFBeUIsWUFBekIsQUFBcUMsQUFDckM7SUFBQSxBQUFFLHVCQUFGLEFBQXlCLFlBQXpCLEFBQXFDLEFBQ3JDO0lBQUEsQUFBRSxvQkFBRixBQUFzQixZQUF0QixBQUFrQyxBQUNsQztJQUFBLEFBQUUsb0JBQUYsQUFBc0IsWUFKdkIsQUFJQyxBQUFrQyxBQUNsQyxBQUNEOztHQUFBLEFBQUUsdUJBQUYsQUFBeUIsTUFBTSxZQUFXLEFBQ3pDO0lBQUEsQUFBRSx1QkFBRixBQUF5QixZQUF6QixBQUFxQyxBQUNyQztJQUFBLEFBQUUsdUJBQUYsQUFBeUIsWUFBekIsQUFBcUMsQUFDckM7SUFBQSxBQUFFLG9CQUFGLEFBQXNCLFlBQXRCLEFBQWtDLEFBQ2xDO0lBQUEsQUFBRSxvQkFBRixBQUFzQixZQUp2QixBQUlDLEFBQWtDLEFBQ2xDLEFBQ0Q7O0dBQUEsQUFBRSx1QkFBRixBQUF5QixNQUFNLFlBQVcsQUFDekM7SUFBQSxBQUFFLHVCQUFGLEFBQXlCLFlBQXpCLEFBQXFDLEFBQ3JDO0lBQUEsQUFBRSx1QkFBRixBQUF5QixZQUF6QixBQUFxQyxBQUNyQztJQUFBLEFBQUUsb0JBQUYsQUFBc0IsWUFIdkIsQUFHQyxBQUFrQyxBQUNsQyxBQUVEO0FBRUE7Ozs7R0FBQSxBQUFFLHNCQUFGLEFBQXdCLE1BQU0sWUFBWSxBQUN6QztJQUFBLEFBQUUsTUFBRixBQUFRLFlBQVIsQUFBb0IsQUFDcEI7SUFBQSxBQUFFLE1BQUYsQUFBUSxZQUZULEFBRUMsQUFBb0IsQUFDcEIsQUFFRDs7O0dBQUEsQUFBRSxjQUFGLEFBQWdCLE1BQU0sWUFBWSxBQUNqQztJQUFBLEFBQUUsY0FBRixBQUFnQixZQUFoQixBQUE0QixBQUM1QjtJQUFBLEFBQUUsY0FBRixBQUFnQixZQUZqQixBQUVDLEFBQTRCLEFBQzVCLEFBRUQ7OztHQUFBLEFBQUUsY0FBRixBQUFnQixNQUFNLFlBQVksQUFDakM7SUFBQSxBQUFFLGNBQUYsQUFBZ0IsWUFBaEIsQUFBNEIsQUFDNUI7SUFBQSxBQUFFLGNBQUYsQUFBZ0IsWUFGakIsQUFFQyxBQUE0QixBQUM1QixBQUVEOzs7R0FBQSxBQUFFLGNBQUYsQUFBZ0IsTUFBTSxZQUFZLEFBQ2pDO0lBQUEsQUFBRSxjQUFGLEFBQWdCLFlBQWhCLEFBQTRCLEFBQzVCO0lBQUEsQUFBRSxjQUFGLEFBQWdCLFlBRmpCLEFBRUMsQUFBNEIsQUFDNUIsQUFFRDs7O0dBQUEsQUFBRSxjQUFGLEFBQWdCLE1BQU0sWUFBWSxBQUNqQztJQUFBLEFBQUUsY0FBRixBQUFnQixZQUFoQixBQUE0QixBQUM1QjtJQUFBLEFBQUUsY0FBRixBQUFnQixZQUZqQixBQUVDLEFBQTRCLEFBQzVCLEFBRUQ7OztHQUFBLEFBQUUscUJBQUYsQUFBdUIsS0FBdkIsQUFBNEIsV0FBNUIsQUFBdUMsQUFFdkMsQUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7O0FBQ0E7QUFDQTtBQUVBOzs7R0FBQSxBQUFFLGVBQUYsQUFBaUIsTUFBTSxZQUFXLEFBQ2pDO1VBQUEsQUFBUSxJQUFSLEFBQVksQUFDWjtJQUFBLEFBQUUsbUJBQUYsQUFBcUIsWUFGdEIsQUFFQyxBQUFpQyxBQUNqQyxBQUVEOzs7R0FBQSxBQUFFLHlCQUFGLEFBQTJCLFNBQTNCLEFBQW9DLE1BQXBDLEFBQTBDLEFBQzFDO0dBQUEsQUFBRSx5QkFBRixBQUEyQixTQUEzQixBQUFvQyxNQUFwQyxBQUEwQyxBQUUxQzs7S0FBSSxVQUFhLEVBQWpCLEFBQWlCLEFBQUUsQUFDbkI7S0FBSSxtQkFBSixBQUF1QixBQUV2Qjs7S0FBSSxtQkFBSixBQUF1QixBQUN2QjtLQUFJLGlCQUFKLEFBQXNCLEFBRXRCOztLQUFJLGdCQUFrQixFQUF0QixBQUFzQixBQUFFLEFBRXhCOztHQUFBLEFBQUUsZUFBRixBQUFpQixTQUFqQixBQUEwQixBQUUxQjs7R0FBQSxBQUFFLFNBQUYsQUFBVyxTQUFYLEFBQW9CLEFBRXBCOztHQUFBLEFBQUUsU0FBRixBQUFXLE1BQU0sVUFBQSxBQUFTLEdBQUUsQUFDM0I7SUFBQSxBQUFFLFNBQUYsQUFBVyxZQUFYLEFBQXVCLEFBRXZCOztJQUFBLEFBQUUsTUFBRixBQUFRLFNBSFQsQUFHQyxBQUFpQixBQUVqQixBQUVEO0FBQ0E7OztHQUFBLEFBQUUsbUJBQUYsQUFBcUIsWUFBckIsQUFBaUMsQUFHakMsQUFFQTs7QUFDQTs7QUFFQTs7O0tBQUksVUFBWSxFQUFoQixBQUFnQixBQUFFLEFBQ2xCO0tBQUksU0FBSixBQUFlLEFBQ2Y7S0FBSSxRQUFKLEFBQWUsQUFDZjtLQUFJLGVBQUosQUFBbUIsQUFDbkIsQUFDQTtBQUVBO0FBQ0E7O0FBRUE7OztHQUFBLEFBQUUsa0RBQUYsQUFBb0QsUUFBcEQsQUFBNEQsQUFDNUQ7R0FBQSxBQUFFLGlCQUFGLEFBQW1CLEtBQW5CLEFBQXdCLFFBQXhCLEFBQWdDLEFBR2hDLEFBRUE7Ozs7R0FBQSxBQUFFLFdBQUYsQUFBYSxNQUFNLFlBQVksQUFDN0I7U0FBQSxBQUFPLFdBRFQsQUFDRSxBQUFrQixBQUNuQixBQUVEO0FBRUE7Ozs7S0FBSSxnQkFBSixBQUFvQixBQUNwQjtLQUFJLFlBQVksRUFBaEIsQUFBZ0IsQUFBRSxBQUNsQjtLQUFJLGNBQWMsRUFBbEIsQUFBa0IsQUFBRSxBQUVwQjs7R0FBQSxBQUFFLGlCQUFGLEFBQW1CLE9BQW5CLEFBQTBCLEFBQzFCO0dBQUEsQUFBRSxrQkFBRixBQUFvQixPQUFwQixBQUEyQixXQUEzQixBQUFzQyxBQUV0QyxBQUVBOzs7O0tBQUksV0FBVyxFQUFmLEFBQWUsQUFBRSxBQUNqQjtLQUFJLFVBQUosQUFBYyxBQUNkO0tBQUksV0FBSixBQUFlLEFBRWQ7O0dBQUEsQUFBRSxVQUFGLEFBQVksTUFBTSxZQUFXLEFBQzVCO0lBQUEsQUFBRSxNQUFGLEFBQVEsWUFBUixBQUFvQixBQUNwQjtJQUFBLEFBQUUsTUFBRixBQUFRLFlBRlQsQUFFQyxBQUFvQixBQUNwQixBQUVGO0FBRUE7Ozs7S0FBSSxjQUFjLEVBQWxCLEFBQWtCLEFBQUUsQUFDcEI7S0FBSSxXQUFXLEVBQWYsQUFBZSxBQUFFLEFBRWpCOztHQUFBLEFBQUUsYUFBRixBQUFlLEtBQWYsQUFBb0IsUUFBcEIsQUFBNEIsQUFDNUI7R0FBQSxBQUFFLFVBQUYsQUFBWSxLQUFaLEFBQWlCLFFBbExsQixBQWtMQyxBQUF5QixBQUd6QiIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJqUXVlcnkoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xyXG5cclxuXHR2YXIgdXJsIFx0XHRcdD1cdHdpbmRvdy5sb2NhdGlvbi5ob3N0O1xyXG5cdHZhciBicmFuZFVybFx0XHQ9XHQnL3dwLWNvbnRlbnQvdGhlbWVzL2Npc3ppLXRoZW1lL2Fzc2V0cy9pbWFnZXMvbG9nb192ZXJkZS5zdmcnO1xyXG5cdHZhciBicmFuZExvZ28gXHRcdD1cdGJyYW5kVXJsO1xyXG5cdFxyXG5cdHZhciBzaXRlU2VhcmNoXHRcdD1cdCQoJy5zaXRlLXNlYXJjaCcpO1xyXG5cdHZhciBzZWFyY2hQbGFjZUhvbGRlciA9ICQoJyN3b29jb21tZXJjZS1wcm9kdWN0LXNlYXJjaC1maWVsZCcpLmF0dHIoJ3BsYWNlaG9sZGVyJyk7XHJcblx0dmFyIHNlYXJjaERpc2FibGVcdD0gJ3BsYWNlaG9sZGVyLXNlYXJjaC1maWVsZC1kaXNhYmxlJztcclxuXHR2YXIgc2VhcmNoQWN0aXZlXHQ9ICdwbGFjZWhvbGRlci1zZWFyY2gtZmllbGQtYWN0aXZlJztcclxuXHRcclxuXHR2YXIgc2VwYXJhdG9yIFx0XHQ9ICc8aW1nIGhyZWY9XCIuLi8uLi9jaXN6aS10aGVtZS9hc3NldHMvaW1hZ2VzL21lbnUtZG90cy5wbmdcIi8+JztcclxuXHJcblx0JCgnLnNpdGUtc2VhcmNoJykuaG92ZXIoZnVuY3Rpb24oKXtcclxuXHRcdCQodGhpcykudG9nZ2xlQ2xhc3MoJ2FjdGl2ZWJveCcpO1xyXG5cdH0pO1xyXG5cclxuXHQkKCcjd29vY29tbWVyY2UtcHJvZHVjdC1zZWFyY2gtZmllbGQnKS5hdHRyKCdwbGFjZWhvbGRlcicsICdCdXNjYWRvcicpO1xyXG5cdCQoJyN3b29jb21tZXJjZS1wcm9kdWN0LXNlYXJjaC1maWVsZCcpLmFkZENsYXNzKHNlYXJjaERpc2FibGUpO1xyXG5cclxuXHQkKCcjd29vY29tbWVyY2UtcHJvZHVjdC1zZWFyY2gtZmllbGQnKS5ob3ZlcihmdW5jdGlvbigpe1xyXG5cdFx0JCgnI3dvb2NvbW1lcmNlLXByb2R1Y3Qtc2VhcmNoLWZpZWxkJykudG9nZ2xlQ2xhc3Moc2VhcmNoRGlzYWJsZSk7XHJcblx0XHQkKCcjd29vY29tbWVyY2UtcHJvZHVjdC1zZWFyY2gtZmllbGQnKS50b2dnbGVDbGFzcyhzZWFyY2hBY3RpdmUpO1x0XHJcblx0fSk7XHJcblx0XHJcblx0JCgnI21lbnUtcHJpbWFyeSA+bGkgPiBhJykuYXBwZW5kKHNlcGFyYXRvcik7XHJcblxyXG5cdCQoJyNmb290ZXJfYnJhbmRMb2dvJykuYXR0cignc3JjJywgYnJhbmRMb2dvKTtcclxuXHJcblx0Ly8gSE9NRSA6IEZVTkNUSU9OIEFCT1VUIEFOSU1BVElPTiBTRVJWSUNFU1xyXG5cclxuXHQkKCcuc2Vydi1pdGVtLWxheW91dC0xJykuaG92ZXIoZnVuY3Rpb24oKSB7XHJcblx0XHQkKCcuc2Vydi1pdGVtLWxheW91dC0xJykudG9nZ2xlQ2xhc3MoJ3NlcnYtbGF5b3V0LW9uJyk7XHJcblx0XHQkKCcuc2Vydi1pdGVtLWxheW91dC0xJykudG9nZ2xlQ2xhc3MoJ3NlcnYtbGF5b3V0LW9mZicpO1xyXG5cdFx0JCgnLnNlcnYtaXRlbS10eHQtMScpLnRvZ2dsZUNsYXNzKCdzZXJ2LWl0ZW0tdHh0LXVwJyk7XHJcblx0XHQkKCcuc2Vydi1pdGVtLXR4dC0xJykudG9nZ2xlQ2xhc3MoJ3NlcnYtaXRlbS10eHQtZG93bicpO1xyXG5cdH0pXHJcblx0JCgnLnNlcnYtaXRlbS1sYXlvdXQtMicpLmhvdmVyKGZ1bmN0aW9uKCkge1xyXG5cdFx0JCgnLnNlcnYtaXRlbS1sYXlvdXQtMicpLnRvZ2dsZUNsYXNzKCdzZXJ2LWxheW91dC1vbicpO1xyXG5cdFx0JCgnLnNlcnYtaXRlbS1sYXlvdXQtMicpLnRvZ2dsZUNsYXNzKCdzZXJ2LWxheW91dC1vZmYnKTtcclxuXHRcdCQoJy5zZXJ2LWl0ZW0tdHh0LTInKS50b2dnbGVDbGFzcygnc2Vydi1pdGVtLXR4dC11cCcpO1xyXG5cdFx0JCgnLnNlcnYtaXRlbS10eHQtMicpLnRvZ2dsZUNsYXNzKCdzZXJ2LWl0ZW0tdHh0LWRvd24nKTtcdFx0XHJcblx0fSlcclxuXHQkKCcuc2Vydi1pdGVtLWxheW91dC0zJykuaG92ZXIoZnVuY3Rpb24oKSB7XHJcblx0XHQkKCcuc2Vydi1pdGVtLWxheW91dC0zJykudG9nZ2xlQ2xhc3MoJ3NlcnYtbGF5b3V0LW9uJyk7XHJcblx0XHQkKCcuc2Vydi1pdGVtLWxheW91dC0zJykudG9nZ2xlQ2xhc3MoJ3NlcnYtbGF5b3V0LW9mZicpO1xyXG5cdFx0JCgnLnNlcnYtaXRlbS10eHQtMycpLnRvZ2dsZUNsYXNzKCdzZXJ2LWl0ZW0tdHh0LXVwJyk7XHJcblx0XHQkKCcuc2Vydi1pdGVtLXR4dC0zJykudG9nZ2xlQ2xhc3MoJ3NlcnYtaXRlbS10eHQtZG93bicpO1xyXG5cdH0pXHRcclxuXHQkKCcuc2Vydi1pdGVtLWxheW91dC00JykuaG92ZXIoZnVuY3Rpb24oKSB7XHJcblx0XHQkKCcuc2Vydi1pdGVtLWxheW91dC00JykudG9nZ2xlQ2xhc3MoJ3NlcnYtbGF5b3V0LW9uJyk7XHJcblx0XHQkKCcuc2Vydi1pdGVtLWxheW91dC00JykudG9nZ2xlQ2xhc3MoJ3NlcnYtbGF5b3V0LW9mZicpO1xyXG5cdFx0JCgnLnNlcnYtaXRlbS10eHQtNCcpLnRvZ2dsZUNsYXNzKCdzZXJ2LWl0ZW0tdHh0LWRvd24nKTtcclxuXHR9KVxyXG5cclxuXHQvLyBMSUJSRVJJQSA6IEZVTkNUSU9OIEFCT1VUIEFOSU1BVElPTiBMSUJSQVJZXHJcblxyXG5cdCQoJy5saWItaXRlbS10eHQtbWFpbicpLmhvdmVyKGZ1bmN0aW9uICgpIHtcclxuXHRcdCQodGhpcykudG9nZ2xlQ2xhc3MoJ2l0ZW0tdHh0LW1haW4tc2hvdycpO1xyXG5cdFx0JCh0aGlzKS50b2dnbGVDbGFzcygnaXRlbS10eHQtbWFpbi1oaWRlJyk7XHJcblx0fSk7XHJcblxyXG5cdCQoJyNpdGVtX2xpYjInKS5ob3ZlcihmdW5jdGlvbiAoKSB7XHJcblx0XHQkKCcjaXRlbV9saWIyJykudG9nZ2xlQ2xhc3MoJ2l0ZW0tdHh0LW1haW4tc2hvdycpO1xyXG5cdFx0JCgnI2l0ZW1fbGliMicpLnRvZ2dsZUNsYXNzKCdpdGVtLXR4dC1tYWluLWhpZGUnKTtcclxuXHR9KTtcclxuXHRcclxuXHQkKCcjaXRlbV9saWIzJykuaG92ZXIoZnVuY3Rpb24gKCkge1xyXG5cdFx0JCgnI2l0ZW1fbGliMycpLnRvZ2dsZUNsYXNzKCdpdGVtLXR4dC1tYWluLXNob3cnKTtcclxuXHRcdCQoJyNpdGVtX2xpYjMnKS50b2dnbGVDbGFzcygnaXRlbS10eHQtbWFpbi1oaWRlJyk7XHJcblx0fSk7XHRcclxuXHRcclxuXHQkKCcjaXRlbV9saWI0JykuaG92ZXIoZnVuY3Rpb24gKCkge1xyXG5cdFx0JCgnI2l0ZW1fbGliNCcpLnRvZ2dsZUNsYXNzKCdpdGVtLXR4dC1tYWluLXNob3cnKTtcclxuXHRcdCQoJyNpdGVtX2xpYjQnKS50b2dnbGVDbGFzcygnaXRlbS10eHQtbWFpbi1oaWRlJyk7XHJcblx0fSk7XHJcblxyXG5cdCQoJyNpdGVtX2xpYjUnKS5ob3ZlcihmdW5jdGlvbiAoKSB7XHJcblx0XHQkKCcjaXRlbV9saWI1JykudG9nZ2xlQ2xhc3MoJ2l0ZW0tdHh0LW1haW4tc2hvdycpO1xyXG5cdFx0JCgnI2l0ZW1fbGliNScpLnRvZ2dsZUNsYXNzKCdpdGVtLXR4dC1tYWluLWhpZGUnKTtcclxuXHR9KTtcclxuXHJcblx0JCgnaW5wdXRbdHlwZT1yYWRpb10nKS5hdHRyKCdjaGVja2VkJywgJ2NoZWNrZWQnKTtcclxuXHJcblx0Ly8gaG9yaXpvbnRhbCBzY3JvbGxcdFx0XHRcdFx0XHJcblx0Ly8gJChcIiNtYWtlTWVTY3JvbGxhYmxlXCIpLnNtb290aERpdlNjcm9sbCh7XHJcblx0Ly8gXHRtb3VzZXdoZWVsU2Nyb2xsaW5nOiBcImFsbERpcmVjdGlvbnNcIixcclxuXHQvLyBcdG1hbnVhbENvbnRpbnVvdXNTY3JvbGxpbmc6IHRydWUsXHJcblx0Ly8gXHRhdXRvU2Nyb2xsaW5nTW9kZTogXCJvblN0YXJ0XCJcclxuXHQvLyB9KTtcdFxyXG5cclxuXHQvLyBpZigkKFwiI2NvbnRlbmVkb3JhamF4XCIpLmh0bWwoKSA9PSBcIlwiKXsgXHJcblx0Ly8gXHQkKFwiI2NvbnRlbmVkb3JhamF4XCIpLmFkZENsYXNzKCdhamF4SGVpZ2h0Jyk7XHJcblx0Ly8gXHQvL2FsZXJ0KFwiTm8gZXhpc3RlbiBlbGVtZW50b3NcIik7IFxyXG5cdC8vIH0gIFxyXG5cdFxyXG5cdCQoXCIudXNlYXJjaGJ0blwiKS5jbGljayhmdW5jdGlvbigpIHtcclxuXHRcdGNvbnNvbGUubG9nKCdmdW5jaW9uYScpO1xyXG5cdFx0JChcIiNjb250ZW5lZG9yYWpheFwiKS5yZW1vdmVDbGFzcygnYWpheEhlaWdodCcpO1x0XHRcclxuXHR9KTtcclxuXHRcclxuXHQkKCcuYmFyLXBvc3RGb3JtYXQtYXVkaW8nKS5hZGRDbGFzcygnZmEnLCAnZmEtdm9sdW1lLWRvd24nKTtcclxuXHQkKCcuYmFyLXBvc3RGb3JtYXQtdmlkZW8nKS5hZGRDbGFzcygnZmEnLCAnZmEtdmlkZW8tY2FtZXJhJyk7XHJcblxyXG5cdHZhciBidG5BamF4XHRcdFx0XHQ9ICQoJy51c2VhcmNoYnRuJyk7XHJcblx0dmFyIGJ0bkFqYXhfY2xhc3Nfb25cdD0gJ3VzZWFyY2hidG4tb24nO1xyXG5cdFxyXG5cdHZhciBidG5BamF4X29ic2VydmVyXHQ9ICdidG5BamF4LW9ic2VydmVyJztcclxuXHR2YXIgYnRuQWpheF9lc3RpbG9cdFx0PSAndXNmYnRuLWFjdGl2ZUJhcic7XHJcblx0XHJcblx0dmFyIGRlZmF1bHRBY3RpdmUgXHRcdD0gJCgnI3V3cHFzZmZyb21fMTg1ID4gI3V3cHFzZl9idG4gPiAudXNlYXJjaGJ0bicpO1xyXG5cclxuXHQkKGRlZmF1bHRBY3RpdmUpLmFkZENsYXNzKCd1c2ZidG4tYWN0aXZlQmFyJyk7XHJcblxyXG5cdCQoYnRuQWpheCkuYWRkQ2xhc3MoYnRuQWpheF9vYnNlcnZlcik7XHJcblxyXG5cdCQoYnRuQWpheCkuY2xpY2soZnVuY3Rpb24oZSl7XHJcblx0XHQkKGJ0bkFqYXgpLnJlbW92ZUNsYXNzKGJ0bkFqYXhfZXN0aWxvKTtcclxuXHRcclxuXHRcdCQodGhpcykuYWRkQ2xhc3MoYnRuQWpheF9lc3RpbG8pO1xyXG5cclxuXHR9KTtcclxuXHJcblx0Ly9SRU1PVkUgQ0xBU1MgRkEgPiBERUZBVUxUIEJPWCBQT1NUXHJcblx0JCgnLmJhci1mb3JtYXRQb3N0JykucmVtb3ZlQ2xhc3MoJ2ZhJyk7XHJcblxyXG5cclxuXHQvL0FERCBNRU5VIE1ZIExJQlJBUllcclxuXHJcblx0Ly88bGkgY2xhc3M9XCJ3b29jb21tZXJjZS1NeUFjY291bnQtbmF2aWdhdGlvbi1saW5rIHdvb2NvbW1lcmNlLU15QWNjb3VudC1uYXZpZ2F0aW9uLWxpbmstLW9yZGVyc1wiPlxyXG5cdC8vIDxhIGhyZWY9XCJodHRwOi8vbG9jYWxob3N0L3Rlc3QudHVwaW5vLmNvbS9jaXN6aS9taS1jdWVudGEvb3JkZXJzL1wiPlBlZGlkb3M8L2E+PC9saT5cclxuXHJcblx0dmFyIGRzaE1lbnUgXHRcdD0gJCgnLndvb2NvbW1lcmNlLU15QWNjb3VudC1uYXZpZ2F0aW9uLWxpbmstLW9yZGVycycpO1xyXG5cdHZhciBkc2hBZG1cdFx0XHQ9ICdIZWxsbyc7XHJcblx0dmFyIGRzaExpIFx0XHRcdD0gJzxsaSBjbGFzcz1cIndvb2NvbW1lcmNlLU15QWNjb3VudC1uYXZpZ2F0aW9uLWxpbmsgd29vY29tbWVyY2UtTXlBY2NvdW50LW5hdmlnYXRpb24tbGluay0tbXktZm9sZGVyc1wiPjxhIGlkPVwiY2lzemlGb2xkZXJzXCIgaHJlZj1cIiNcIj5NaSBMaWJyZXJpYTwvYT48L2xpPic7XHJcblx0dmFyIGNpc3ppRm9sZGVyc1x0PSAnLi4vbWktbGlicmVyaWEnO1xyXG5cdC8vIHZhciBjcnNfbWVudV9pdG0gXHQ9ICc8bGkgY2xhc3M9XCJ3b29jb21tZXJjZS1NeUFjY291bnQtbmF2aWdhdGlvbi1saW5rIHdvb2NvbW1lcmNlLU15QWNjb3VudC1uYXZpZ2F0aW9uLWxpbmstLW15LWNvdXJzZXNcIj48YSBpZD1cImNyc19tZW51X2l0bVwiIGhyZWY9XCIjXCI+TWlzIEN1cnNvczwvYT48L2xpPic7XHJcblx0Ly8gdmFyIGNyc19saW5rXHRcdD0gJ2h0dHA6Ly9sb2NhbGhvc3QvdGVzdC50dXBpbm8uY29tL2theWJwbS1tYXQvbWlzLWN1cnNvcy8nO1xyXG5cclxuXHQvLyAkKCcud29vY29tbWVyY2UtTXlBY2NvdW50LW5hdmlnYXRpb24gdWwnKS5wcmVwZW5kKGNyc19tZW51X2l0bSk7XHJcblx0Ly8gJCgnI2Nyc19tZW51X2l0bScpLmF0dHIoJ2hyZWYnLCBcIi4vLi4vbWlzLWN1cnNvc1wiKTtcclxuXHJcblx0JCgnLndvb2NvbW1lcmNlLU15QWNjb3VudC1uYXZpZ2F0aW9uLWxpbmstLW9yZGVycycpLnByZXBlbmQoZHNoTGkpO1xyXG5cdCQoJyNjaXN6aUZvbGRlcnMnKS5hdHRyKCdocmVmJywgXCIuLi9taS1saWJyZXJpYVwiKTtcclxuXHJcblxyXG5cdC8vQ1VTVE9NIEZPT1RFUiBNRU5VXHJcblxyXG5cdCQoJy5zZWFyY2gnKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcblx0XHQgd2luZG93LmxvY2F0aW9uID0gXCIuL21pLWxpYnJlcmlhXCI7XHJcblx0fSlcclxuXHJcblx0Ly9DVVNUT00gSEVBREVSIFBPU1RcclxuXHJcblx0bGV0IGhlYWRfcG9zdF9kaXYgPSAnPGRpdiBpZD1cImhlYWRfcG9zdF9kaXZcIj48L2Rpdj4nO1xyXG5cdGxldCBwb3N0ZWRfb24gPSAkKCcucG9zdGVkLW9uJyk7XHJcblx0bGV0IHBvc3RfYXV0aG9yID0gJCgnLmF1dGhvcicpO1xyXG5cclxuXHQkKCcuZW50cnktaGVhZGVyJykuYXBwZW5kKGhlYWRfcG9zdF9kaXYpO1xyXG5cdCQoJyNoZWFkX3Bvc3RfZGl2JykuYXBwZW5kKHBvc3RlZF9vbiwgcG9zdF9hdXRob3IpO1xyXG5cclxuXHQvL0NVU1RPTSBDQVJEIFBPU1RTXHJcblxyXG5cdGxldCBjYXJkX3R4dCA9ICQoJy5jYXJkLWl0ZW0tdHh0Jyk7XHJcblx0bGV0IGNhcmRfb24gPSAnY2FyZC1pdGVtLXR4dC1vbic7XHJcblx0bGV0IGNhcmRfb2ZmID0gJ2NhcmQtaXRlbS10eHQtb2ZmJztcclxuXHJcblx0XHQkKGNhcmRfdHh0KS5ob3ZlcihmdW5jdGlvbiAoKXtcclxuXHRcdFx0JCh0aGlzKS50b2dnbGVDbGFzcyhjYXJkX29uKTtcclxuXHRcdFx0JCh0aGlzKS50b2dnbGVDbGFzcyhjYXJkX29mZik7XHJcblx0XHR9KTtcclxuXHJcblx0Ly9QT1NUXHJcblxyXG5cdGxldCBhdXRob3JfbGluayA9ICQoJy5hdXRob3IgPiBhJyk7XHJcblx0bGV0IGNhdF9saW5rID0gJCgnLmNhdC1saW5rcyA+IGEnKTtcclxuXHJcblx0JChhdXRob3JfbGluaykuYXR0cignaHJlZicsICcjJyk7XHJcblx0JChjYXRfbGluaykuYXR0cignaHJlZicsICcjJyk7XHJcblxyXG5cclxufSkiXX0=
