
<?php /**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: CISZI Filosofia
 *
 * @package ciszi-theme
 */
get_header(); ?>
<div id="primary" class="conten-area">
  <main id="main" role="main" class="site-main">
    <section id="section-philosofy" class="row">
      <div class="col-lg-7 col-md-7 col-sm-8 col-xs-12 filosofia-txt mi-filosofia-txt">
        <article>
          <?php the_field(mi_filosofia); ?>
        </article>
      </div>
      <div class="col-lg-7 col-md-7 col-sm-8 col-xs-12 filosofia-ico mi-filosofia-ico"><img src="../wp-content/themes/ciszi-theme/assets/images/brand-blue.png" ALT="Ciszi Vera Web"/></div>
    </section>
    <section id="section-acerca" class="row">
      <div class="col-lg-offset-5 col-md-offset-5 col-sm-offset-5 col-lg-7 col-md-7 col-sm-7 col-xs-12 filosofia-txt acerca-txt">
        <article>
          <?php the_field(acerca_de_mi)		; ?>
        </article>
      </div>
      <div class="col-lg-offset-5 col-md-offset-5 col-sm-offset-5 col-lg-7 col-md-7 col-sm-7 col-xs-12 filosofia-ico acerca-ico"><img src="../wp-content/themes/ciszi-theme/assets/images/brand-blue.png" ALT="Ciszi Vera Web"/></div>
    </section>
    <section id="section-biografia" class="row">
      <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 filosofia-txt biografia-txt">
        <article>
          <?php the_field(biografia)		; ?>
        </article>
      </div>
      <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 filosofia-ico biografia-ico"><img src="../wp-content/themes/ciszi-theme/assets/images/brand-blue.png" ALT="Ciszi Vera Web"/></div>
    </section>
  </main>
</div>
<?php get_footer(); ?>