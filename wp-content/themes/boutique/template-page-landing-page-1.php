
<?php /**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: CISZI Landing 1
 *
 * @package ciszi-theme
 */
get_header(); ?>
<div class="landing-header-section">
  <h1>Texto de Prueba</h1>
</div>
<div id="primary" class="conten-area">
  <main id="main" role="main" class="site-main">
    <div style="margin-top: 1%" class="landing-page-ciszi-banner"><img src="<?php the_field(landing_page_banner) ?>" alt="Eventos"/></div>
    <div class="landing-page-main landing-page-1">
      <div class="landing-page-section1 landing-page-row row">
        <div class="landing-page-col col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="landing-page-col-excerpt">
            <?php echo do_shortcode(the_field(landing_page_1_text_1)); ?>
          </div>
        </div>
        <div class="landing-page-col col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <h3>Registrate ahora, es gratis! </h3>
          <h5>Deja tu e-mail para conseguir más información.</h5>
          <div class="landing-page-col-register">
            <?php echo do_shortcode(the_field(landing_page_1_ciszi_form)); ?>
          </div>
        </div>
      </div>
      <div class="landing-page-section2 landing-page-row row">
        <div class="landing-page-col col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="landing-page-col-excerpt">
            <?php echo do_shortcode(the_field(landing_page_1_ciszi_text_2)); ?>
          </div>
        </div>
        <div class="landing-page-col col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="landing-page-col-image"><img src="<?php the_field(landing_page_1_img); ?>"/></div>
        </div>
      </div>
      <div class="landing-page-section3 landing-page-row row">
        <div class="landing-page-col col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="landing-page-col-video">
            <?php the_field(landing_page_video); ?>
          </div>
        </div>
        <div class="landing-page-col col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="landing-page-col-icos">
            <div class="col-ico-1 col-ico-audio"><i class="fa fa-volume-up"></i><a href="">
                <h3>Escucha lo que tenemos que decirte</h3></a></div>
            <div class="col-ico-2 col-ico-pdf"><i class="fa fa-file-pdf-o"></i><a href="">
                <h3>Enterate más aquí</h3></a></div>
          </div>
        </div>
      </div>
    </div>
  </main>
</div>
<?php get_footer(); ?>