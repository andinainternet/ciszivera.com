
<?php /**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: CISZI Blog page
 *
 * @package ciszi-theme
 */
get_header(); ?>
<div id="primary" class="conten-area">
  <main id="main" role="main" class="site-main">
    <div class="col-lg-12">
      <h2>Columna Customizada</h2>
    </div>
    <div class="col-lg-12">
      <h2>Parte del Blog Customizado</h2>
      <?php do_action( 'storefront_loop_before' );
while ( have_posts() ) : the_post();
	get_template_part( 'content', get_post_format() );
endwhile;
do_action( 'storefront_loop_after' ); ?>
    </div>
  </main>
</div>
<?php get_footer(); ?>