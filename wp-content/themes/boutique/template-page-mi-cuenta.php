
<?php /**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: CISZI Mi Cuenta
 *
 * @package ciszi-theme
 */
get_header(); ?>
<div id="primary" class="conten-area">
  <main id="main" role="main" class="site-main accountPage-Ciszi">
    <?php if ( is_page('mi-cuenta') ) {
	if ( is_user_logged_in() ){
		$miCuenta = '<div class="titleAccount-loggedIn"><h1>Mi Cuenta</h1></div>';
		echo $miCuenta;
	} else{
		$hazteMiembro = '<div class="titleAccount-loggedOut"><h1>Hazte Miembro</h1></div>';
		echo $hazteMiembro;
	}
} else {}; ?>
    <?php while ( have_posts() ) : the_post();
do_action( 'storefront_page_before' );
get_template_part( 'content', 'page' );
/**
* Functions hooked in to storefront_page_after action
*
* @hooked storefront_display_comments - 10
*/
do_action( 'storefront_page_after' );
endwhile; // End of the loop.; ?>
  </main>
</div>
<?php get_footer(); ?>