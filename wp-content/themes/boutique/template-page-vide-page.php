
<?php /**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: CISZI Video Page
 *
 * @package ciszi-theme
 */
get_header(); ?>
<div id="primary" class="conten-area">
  <main id="main" role="main" class="site-main">
    <section id="category_bg" class="single-page-category-bg">
      <div class="single-page-main"><img src="<?php the_field(single_page_img_principal_desktop); ?>" alt="Background" class="single-page-item-img hidden-xs"/><img src="<?php the_field(single_page_img_principal_mobile); ?>" alt="Background" class="single-page-item-img hidden-lg hidden-md hidden-sm"/></div>
    </section>
    <section id="category_content">
      <div class="category-page-main col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="category-page-content col-lg-8 col-md-8 col-sm-12 col-xs-12">
          <?php do_action( 'storefront_loop_before' );
while ( have_posts() ) : the_post();
	get_template_part( 'content', get_post_format() );
endwhile;
do_action( 'storefront_loop_after' ); ?>
        </div>
        <div class="video-sidebar col-lg-4 col-md-4">
          <div class="video-sidebar-item1"><img src="<?php the_field(video_page_imagen_1);?>" alt=""/></div>
          <div class="video-sidebar-item2"><img src="<?php the_field(video_page_imagen_2);?>" alt=""/></div>
          <div class="video-sidebar-item3"><img src="<?php the_field(video_page_imagen_3);?>" alt=""/></div>
        </div>
      </div>
    </section>
  </main>
</div>
<?php get_footer(); ?>