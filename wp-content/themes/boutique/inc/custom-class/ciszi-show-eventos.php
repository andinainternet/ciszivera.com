<?php 
//=================================
// SHOW EVENTOS
//=================================

function shortcode_custom_eventos($atts, $content = null, $code) {

//Uso: [recientes  limite="3" longitud_titulo="50" longitud_desc="50" thumbnail="1" tamano="50"]
//thumbnail="1" muestra imagen destacada. thumbnail="0" no muestra la imagen
	extract(shortcode_atts(array(
		'limite' => 24,
		'longitud_titulo' => 100,
		'longitud_desc' => 450,
		'thumbnail' => false,
		'tamano' => 350,
		'post_type' => array('eventos_a', 'eventos_b', 'eventos_c')
 
	), $atts));
 
	$query = array(
    				'post_type' => $post_type
    				);
 
	$q = new WP_Query($query);
	if ($q->have_posts()) :
	$salida  = '';
	$salida .= '<div class="eventos-main col-lg-12 col-md-12 col-sm-12 col-xs-12">';
 	/* comienzo while */
	while ($q->have_posts()) : $q->the_post();
	$salida .= '<div class="eventos-card-main-item col-lg-12 col-md-12 col-sm-12 col-xs-12">';
	if ( has_post_thumbnail() && $thumbnail == true):
	
		/*SECTION - IMAGE POST*/
		$salida .= '<div class="eventos-card-ctn-body">';
			$salida .= '<div class="eventos-card-item-img col-lg-3 col-md-3 col-sm-3 col-xs-12">';
				$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
				$salida .= get_the_post_thumbnail(get_the_id(),array($tamano,$tamano),array('title'=>get_the_title(),'alt'=>get_the_title(),'class'=>'imageborder'));
				$salida .= '</a>';
		$salida .= '</div>';
		

		/*SECTION - CONTAINER*/
		$salida .= '<div class="eventos-container col-lg-9 col-md-9 col-sm-9 col-xs-12">';
			/*SECTION - TITLEPOST*/
			$salida .= '<div class="eventos-card-ctn-title">';
				$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'"><h1>';
				$salida .= wp_html_excerpt (get_the_title(), $longitud_titulo, ' ...' ).'';
				$salida .= '</h1></a>';
			// <a href="" class="ciszi-collection ciszi-saveto" data-postid="'.$post_id.'"><span class="glyphicon glyphicon-plus-sign"></span> Guardar en tu libreria</a>				
			// $salida .= '</div>';		
			$salida .= '</div>';

			/*SECTION - DATE PLACE POST*/

			$salida.= '<div class="event-info"><div class="event-info-item"><i class="fa fa-calendar"></i><span>'. get_field("eventos_fecha") .'</span></div><div class="event-info-item" ><i class="fa fa-map-marker fa-col"></i><span>'. get_field("eventos_lugar") .'</span></div></div>';


			/*SECTION - DESCRIPTION POST*/
			$salida .= '<div class="eventos-posts_content eventos-card-item-txt">';
			 	// $salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
					$salida .= '<p>';
					// $salida .= '<h3>'.get_the_title().'</h3>';

			 		/* Escribo extracto  */
				$excerpt = get_the_excerpt();
				$salida .= ($excerpt)?'<p>'.wp_html_excerpt($excerpt,$longitud_desc).'</p>':'';
			 	$salida .= '</p>';
			 	// $salida .= '</a>';
	 		$salida .= '</div>';
	 	$salida .= '</div>';

	
		endif;


	$salida .= '</div>';
	$salida .= '</div>';
	endwhile;
	wp_reset_query();
	/* fin while */
 
	$salida .= '</div>';
	endif;
 
	return $salida;
 
}

add_shortcode('custom_eventos', 'shortcode_custom_eventos');



 ?>