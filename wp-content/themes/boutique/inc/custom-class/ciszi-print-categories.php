<?php 

function wpb_catlist_desc() { 
// $attachment = wp_get_attachment_image_src( $images[$cat_id], $size );
$categories = wp_get_post_categories( $object_id );

$string = '<div id="style-1" class="lib-subcat-main">';

$catlist = get_terms( 'category' );

if ( ! empty( $catlist ) ) {
  foreach ( $catlist as $key => $item ) {

	  	$img_id = $item->term_id;
		$images = cfi_featured_image_url( array( 'cat_id' => $img_id ) );
		$cat_ft_title = cfi_featured_image( array( 'title' => '36' ) );
		$cat_title = 'http://test.tupino.com/ciszi/'. $cat_title;
		$cat_id = intval( $args['cat_id'] );
		$cat_img = get_the_category($image);


	    $string .= '<div class="lib-subcat-item">
						<div class="lib-item-img-main">
							<a href="/'. $item->slug .'"><img src="'. $images .'" /></a>
						</div>
						<div class="lib-item-txt-main item-txt-main-hide">
							<div class="lib-item-txt-title">
								<h5>' . $item->name . '</h5>
				    			<i class="lib-item-txt-icon"></i>
							</div>
							<div class="lib-item-txt-descrp">
			    				<p>'. $item->description . '</p>
			    			</div>
	    				</div>
	    			</div>';
	    			}
}
$string .= '</div>';

return $string; 
}
add_shortcode('wpb_categories', 'wpb_catlist_desc');


 ?>