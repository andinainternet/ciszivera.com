<?php 

function shortcode_post_categories($atts, $content = null, $code) {
 
//Uso: [recientes  limite="3" longitud_titulo="50" longitud_desc="50" thumbnail="1" tamano="50"]
//thumbnail="1" muestra imagen destacada. thumbnail="0" no muestra la imagen
	extract(shortcode_atts(array(
		'limite' => '',
		'longitud_titulo' => 100,
		'longitud_desc' => 450,
		'thumbnail' => false,
		'tamano' => 350,
		'cat' => '',
		'tag' => '' 
	), $atts));
 
	$query = array(	'showposts' => $limite,  
					'orderby'=> 'date', 
					'order'=>'DESC', 
					'post_status' => 
					'publish', 
					'ignore_sticky_posts' => 1, 
					'cat' => $cat,
					'tag' => $tag,

					);
 
	$q = new WP_Query($query);
	if ($q->have_posts()) :
	$salida  = '';
	$salida .= '<div class="listado-videos-recientes col-lg-12 col-md-12 col-sm-12 col-xs-12">';
 
	/* comienzo while */
	while ($q->have_posts()) : $q->the_post();
	$salida .= '<div class="card-main-item col-lg-4 col-md-4 col-sm-6 col-xs-12">';
	if ( has_post_thumbnail() && $thumbnail == true):
	
		/*SECTION - IMAGE POST*/
		$salida .= '<div class="card-ctn-body">';
			$salida .= '<div class="card-item-img">';
				$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
				$salida .= get_the_post_thumbnail(get_the_id(),array($tamano,$tamano),array('title'=>get_the_title(),'alt'=>get_the_title(),'class'=>'imageborder'));
				$salida .= '</a>';
		$salida .= '</div>';
		

		/*SECTION - TITLEPOST*/
		$salida .= '<div class="card-ctn-title">';
			$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
			$salida .= wp_html_excerpt (get_the_title(), $longitud_titulo, ' ...' ).'';
			$salida .= '</a>';
 		
 		// $salida .= '<div class="container-barFormat">';
		// $salida .= '<div class="bar-formatPost bar-formatPost-'.get_post_format($format, $post_id).'"></div>';
			$salida .= '<div class="bar-format-container">
						<div class="bar-formatPost bar-formatPost-'.get_post_format($format, $post_id).'"></div>
						<div class="bar-formatPost-buttonSave">
						</div>
					</div>';
		// <a href="" class="ciszi-collection ciszi-saveto" data-postid="'.$post_id.'"><span class="glyphicon glyphicon-plus-sign"></span> Guardar en tu libreria</a>				
		// $salida .= '</div>';		

		$salida .= '</div>';
		
		/*SECTION - DESCRIPTION POST*/
		$salida .= '<div class="posts_content card-item-txt card-item-txt-off">';
		 	$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
				// $salida .= '<h3>'.get_the_title().'</h3>';

		 		/* Escribo extracto  */
			$excerpt = get_the_excerpt();
			$salida .= ($excerpt)?'<p>'.wp_html_excerpt($excerpt,$longitud_desc).'</p>':'';
		 	$salida .= '</a>';
 		$salida .= '</div>';




	
		endif;


	$salida .= '</div>';
	$salida .= '</div>';
	endwhile;
	wp_reset_query();
	/* fin while */
 
	$salida .= '</div>';
	endif;
 
	return $salida;
 
}

add_shortcode('post_categories', 'shortcode_post_categories');

 ?>