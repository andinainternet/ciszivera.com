<?php 

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_landing-page-1',
		'title' => 'Landing Page 1',
		'fields' => array (
			array (
				'key' => 'field_59176d7ee90bd',
				'label' => 'Sección 1',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_59176d89e90be',
				'label' => 'Texto 1',
				'name' => 'landing_page_1_text_1',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_59176d9de90bf',
				'label' => 'Formulario',
				'name' => 'landing_page_1_ciszi_form',
				'type' => 'wysiwyg',
				'instructions' => 'Aqui copias el <strong><em>Shortcode</em></strong> generado por el formulario de	Contact Form',
				'default_value' => '',
				'toolbar' => 'basic',
				'media_upload' => 'no',
			),
			array (
				'key' => 'field_59176dbbe90c0',
				'label' => 'Sección 2',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_59176dc9e90c1',
				'label' => 'Texto 2',
				'name' => 'landing_page_1_ciszi_text_2',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_59176dd5e90c2',
				'label' => 'Imagen destacada',
				'name' => 'landing_page_1_img',
				'type' => 'image',
				'instructions' => 'Imagen de <em>475px x 300px</em>',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_59176df0704ed',
				'label' => 'Sección 3',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_5917684e7c2c1',
				'label' => 'Video',
				'name' => 'landing_page_video',
				'type' => 'textarea',
				'instructions' => 'Pega aquí el iframe generado en "youtube"',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'html',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-page-landing-page-1.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'eventos_a',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}


 ?>