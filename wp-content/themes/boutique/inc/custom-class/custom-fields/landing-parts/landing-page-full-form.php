<?php 
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_landing-page-full-form',
		'title' => 'Landing Page Full Form',
		'fields' => array (
			array (
				'key' => 'field_5941f7ee3d32b',
				'label' => 'Imagen Destacada',
				'name' => 'full_form_img',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_591b230c37a5f',
				'label' => 'Formulario',
				'name' => 'full_form_form',
				'type' => 'wysiwyg',
				'instructions' => 'Shortcode del Formulario',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-page-landing-page-3.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'eventos_b',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'eventos_c',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-page-landing-page-2.php',
					'order_no' => 0,
					'group_no' => 2,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}



 ?>