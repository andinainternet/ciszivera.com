<?php 

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_landing-page-3',
		'title' => 'Landing Page 3',
		'fields' => array (
			array (
				'key' => 'field_5945f83a28e86',
				'label' => 'Cabecera',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_5945fa0728e87',
				'label' => 'Texto de Cabecera',
				'name' => 'landing3_txt_header',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_591b1ae3bea31',
				'label' => 'Sección 1',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_591b1ac5bea30',
				'label' => 'Texto 1',
				'name' => 'landing_page_3_ciszi_text_1',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_591b1afbbea32',
				'label' => 'Imagen destacada',
				'name' => 'landing_page_3_img_1',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_591b1b0bbea33',
				'label' => 'Sección 2',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_591b1b15bea34',
				'label' => 'Imagen destacada',
				'name' => 'landing_page_3_img_2',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_591b1b2bbea35',
				'label' => 'Video',
				'name' => 'landing_page_3_video',
				'type' => 'wysiwyg',
				'instructions' => 'Pega el iframe generado desde youtube',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_5939a49b5bee0',
				'label' => 'Background Color',
				'name' => 'landing_page_3_bg',
				'type' => 'color_picker',
				'default_value' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-page-landing-page-3.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'eventos_c',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

 ?>