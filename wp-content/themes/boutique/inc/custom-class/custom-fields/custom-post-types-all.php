<?php 

function cptui_register_my_cpts() {

	/**
	 * Post Type: Planes.
	 */

	$labels = array(
		"name" => __( 'Planes', 'boutique' ),
		"singular_name" => __( 'Plan', 'boutique' ),
		"menu_name" => __( 'Planes', 'boutique' ),
		"all_items" => __( 'Todos los planes', 'boutique' ),
		"add_new" => __( 'Agregar Plan', 'boutique' ),
		"add_new_item" => __( 'Agregar nuevo plan', 'boutique' ),
		"edit_item" => __( 'Editar Plan', 'boutique' ),
		"new_item" => __( 'Nuevo Plan', 'boutique' ),
		"view_item" => __( 'Ver Plan', 'boutique' ),
		"view_items" => __( 'Ver Planes', 'boutique' ),
		"search_items" => __( 'Buscar Planes', 'boutique' ),
		"not_found" => __( 'No encontrado', 'boutique' ),
	);

	$args = array(
		"label" => __( 'Planes', 'boutique' ),
		"labels" => $labels,
		"description" => "Planes de Membresia para Ciszi Vera Web",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "planes", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-id-alt",
		"supports" => array( "title", "editor", "thumbnail", "excerpt" ),
	);

	register_post_type( "planes", $args );

	/**
	 * Post Type: Compartir.
	 */

	$labels = array(
		"name" => __( 'Compartir', 'boutique' ),
		"singular_name" => __( 'Compartir', 'boutique' ),
		"menu_name" => __( 'Compartir', 'boutique' ),
		"all_items" => __( 'Todos los items', 'boutique' ),
		"add_new" => __( 'Agregar nuevo', 'boutique' ),
		"add_new_item" => __( 'Agregar nuevo item', 'boutique' ),
		"edit_item" => __( 'Editar item', 'boutique' ),
		"new_item" => __( 'Nuevo item', 'boutique' ),
		"view_item" => __( 'Ver item', 'boutique' ),
		"view_items" => __( 'Ver items', 'boutique' ),
		"search_items" => __( 'Buscar item', 'boutique' ),
		"not_found" => __( 'No encontrado', 'boutique' ),
		"not_found_in_trash" => __( 'No encontrado en la papelera', 'boutique' ),
	);

	$args = array(
		"label" => __( 'Compartir', 'boutique' ),
		"labels" => $labels,
		"description" => "Sección que muestra las imágenes que los usuarios han compartido en base a su experiencia.",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "compartir", "with_front" => true ),
		"query_var" => true,
		"menu_position" => 25,
		"menu_icon" => "dashicons-format-gallery",
		"supports" => array( "title", "editor", "thumbnail", "excerpt", "author" ),
	);

	register_post_type( "compartir", $args );

	/**
	 * Post Type: Eventos A.
	 */

	$labels = array(
		"name" => __( 'Eventos A', 'boutique' ),
		"singular_name" => __( 'Evento A', 'boutique' ),
		"menu_name" => __( 'Eventos A', 'boutique' ),
		"all_items" => __( 'Todos los eventos', 'boutique' ),
		"add_new" => __( 'Agregar nuevo', 'boutique' ),
		"add_new_item" => __( 'Agregar nuevo item', 'boutique' ),
		"edit_item" => __( 'Editar item', 'boutique' ),
		"new_item" => __( 'Nuevo item', 'boutique' ),
		"view_item" => __( 'Ver item', 'boutique' ),
		"view_items" => __( 'Ver items', 'boutique' ),
		"search_items" => __( 'Buscar item', 'boutique' ),
		"not_found" => __( 'No encontrado', 'boutique' ),
		"not_found_in_trash" => __( 'No encontrado en la papelera', 'boutique' ),
		"featured_image" => __( 'Imagen destacada', 'boutique' ),
	);

	$args = array(
		"label" => __( 'Eventos A', 'boutique' ),
		"labels" => $labels,
		"description" => "Eventos realizados por Ciszi Vera",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => true,
		"rewrite" => array( "slug" => "eventos_a", "with_front" => true ),
		"query_var" => true,
		"menu_position" => 26,
		"menu_icon" => "dashicons-megaphone",
		"supports" => array( "title", "editor", "thumbnail", "excerpt", "author" ),
		"taxonomies" => array( "categoria_eventos" ),
	);

	register_post_type( "eventos_a", $args );

	/**
	 * Post Type: Eventos B.
	 */

	$labels = array(
		"name" => __( 'Eventos B', 'boutique' ),
		"singular_name" => __( 'Evento B', 'boutique' ),
		"menu_name" => __( 'Eventos B', 'boutique' ),
		"all_items" => __( 'Todos los eventos', 'boutique' ),
		"add_new" => __( 'Agregar evento', 'boutique' ),
		"edit_item" => __( 'Editar evento', 'boutique' ),
	);

	$args = array(
		"label" => __( 'Eventos B', 'boutique' ),
		"labels" => $labels,
		"description" => "Eventos realizados por Ciszi Vera - Modelo Landing Page B",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => true,
		"rewrite" => array( "slug" => "eventos_b", "with_front" => true ),
		"query_var" => true,
		"menu_position" => 27,
		"menu_icon" => "dashicons-megaphone",
		"supports" => array( "title", "editor", "thumbnail", "excerpt", "author" ),
		"taxonomies" => array( "categoria_eventos" ),
	);

	register_post_type( "eventos_b", $args );

	/**
	 * Post Type: Eventos C.
	 */

	$labels = array(
		"name" => __( 'Eventos C', 'boutique' ),
		"singular_name" => __( 'Evento C', 'boutique' ),
		"menu_name" => __( 'Eventos C', 'boutique' ),
	);

	$args = array(
		"label" => __( 'Eventos C', 'boutique' ),
		"labels" => $labels,
		"description" => "Eventos realizados por Ciszi Vera - Modelo Landing Page C",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => true,
		"rewrite" => array( "slug" => "eventos_c", "with_front" => true ),
		"query_var" => true,
		"menu_position" => 28,
		"menu_icon" => "dashicons-megaphone",
		"supports" => array( "title", "editor", "thumbnail", "author" ),
		"taxonomies" => array( "categoria_eventos" ),
	);

	register_post_type( "eventos_c", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );


 ?>