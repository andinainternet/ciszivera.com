<?php 

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_fecha-eventos',
		'title' => 'Fecha Eventos',
		'fields' => array (
			array (
				'key' => 'field_59516fbd4f78b',
				'label' => 'Fecha ',
				'name' => 'eventos_fecha',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_59516fd44f78c',
				'label' => 'Lugar',
				'name' => 'eventos_lugar',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'eventos_b',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'eventos_c',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}



 ?>