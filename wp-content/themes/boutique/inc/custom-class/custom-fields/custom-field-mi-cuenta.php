<?php 

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_mi-cuenta',
		'title' => 'Mi Cuenta',
		'fields' => array (
			array (
				'key' => 'field_594d678eed437',
				'label' => 'Background',
				'name' => 'background_micuenta',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page',
					'operator' => '==',
					'value' => '7',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}


 ?>