<?php 

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_philosofy',
		'title' => 'Philosofy',
		'fields' => array (
			array (
				'key' => 'field_58d2b45c18200',
				'label' => 'Mi Filosofia',
				'name' => 'mi_filosofia',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_58d404644a28a',
				'label' => 'Acerca de Mi',
				'name' => 'acerca_de_mi',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_58d2b49918202',
				'label' => 'Biografía',
				'name' => 'biografia',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-page-filosofia.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}


 ?>