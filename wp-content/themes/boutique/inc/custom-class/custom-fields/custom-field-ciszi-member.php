<?php 

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_ciszi-membrer',
		'title' => 'Ciszi Membrer',
		'fields' => array (
			array (
				'key' => 'field_58d32a3ec6404',
				'label' => 'Link Plan de Membreria',
				'name' => 'link_plan_1',
				'type' => 'relationship',
				'instructions' => 'Registra el Plan de Membresía que corresponde',
				'return_format' => 'id',
				'post_type' => array (
					0 => 'product',
				),
				'taxonomy' => array (
					0 => 'product_cat:18',
				),
				'filters' => array (
					0 => 'search',
				),
				'result_elements' => array (
					0 => 'post_type',
					1 => 'post_title',
				),
				'max' => '',
			),
			array (
				'key' => 'field_58e67bc493da7',
				'label' => 'Bakground Image Plan',
				'name' => 'planbg_img',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'planes',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}


 ?>