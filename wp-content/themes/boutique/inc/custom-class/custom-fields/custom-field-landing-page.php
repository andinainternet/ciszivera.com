<?php 

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_landing-page',
		'title' => 'Landing Page',
		'fields' => array (
			array (
				'key' => 'field_591768247c2be',
				'label' => 'Sección 1',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_591767c87c2ba',
				'label' => 'Texto 1',
				'name' => 'landing_page_1_text_1',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_591767e27c2bb',
				'label' => 'Formulario ',
				'name' => 'landing_page_1_ciszi_form',
				'type' => 'textarea',
				'instructions' => 'Ingresa el <em>shortcode</em> del formulario de <em>Contact Form</em>',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_591768177c2bd',
				'label' => 'Sección 2',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_5917680d7c2bc',
				'label' => 'Texto 2',
				'name' => 'landing_page_1_ciszi_text_2',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_591768357c2bf',
				'label' => 'Imagen destacada',
				'name' => 'landing_page_1_img',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_591768447c2c0',
				'label' => 'Sección 3',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_5917684e7c2c1',
				'label' => 'Video',
				'name' => 'landing_page_video',
				'type' => 'textarea',
				'instructions' => 'Pega aquí el iframe generado en "youtube"',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'html',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-page-landing-page-1.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}


 ?>