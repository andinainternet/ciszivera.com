<?php 

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_libreria',
		'title' => 'Libreria',
		'fields' => array (
			array (
				'key' => 'field_58d18ff8e71d1',
				'label' => 'Hero Text',
				'name' => 'hero_text',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_591344be33a1f',
				'label' => 'Libreria Image',
				'name' => 'libreria_image_hero',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-page-libreria.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}


 ?>