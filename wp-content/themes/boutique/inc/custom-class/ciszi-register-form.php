<?php 


// output the form field
add_action('register_form', 'ad_register_fields');
	function ad_register_fields() {
	?>
	    <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide register-custom-input-name">
	        <label for="firstname"><?php _e('Nombre') ?><br />
	        <input type="firstname" name="firstname" id="firstname" class="woocommerce-Input woocommerce-Input--text input-text" value="<?php echo esc_attr($_POST['firstname']); ?>" size="100" tabindex="20" />
	        </label>
	    </p>
	<?php
}

// SEX FIELD
add_action('register_form', 'agregar_campos_registro' );
add_filter('registration_errors', 'validar_campos_registro', 10, 3);
add_action('user_register', 'guardar_campos_registro');
 
function agregar_campos_registro() {
	$user_edad = ( isset( $_POST['user_edad'] ) ) ? $_POST['user_edad']: '';
?>
	
	<div class="sex-form-ciszi" style="margin-bottom:20px">
		<label>Sexo</label><br />
		<div class="sex-form-ciszi-checks">
			<label>
				<input type="radio" name="user_sexo" class="radio" value="h" /> <span>Hombre</span> </label>
			<label>
				<input type="radio" name="user_sexo" class="radio" value="m" style="margin-left:10px" /> <span>Mujer</span> </label>
		</div>
	</div>
<?php
}
 
function validar_campos_registro ($errors, $sanitized_user_login, $user_email) {
    if ( empty( $_POST['user_edad'] ) )
        $errors->add( 'user_edad_error', __('<strong>ERROR</strong>: Por favor, introduce tu edad.') );
    return $errors;
}
 
function guardar_campos_registro ($user_id) {
	if ( isset($_POST['user_sexo']) ){
		update_user_meta($user_id, 'user_sexo', $_POST['user_sexo']);
	}
}





// save new first name
add_filter('pre_user_first_name', 'ad_user_firstname');
	function ad_user_firstname($firstname) {
	    if (isset($_POST['firstname'])) {
	        $firstname = $_POST['firstname'];
	    }
	    return $firstname;
	}

//SEX FIELD DASHBOARD

add_action( 'show_user_profile', 'agregar_campos_perfil' );
add_action( 'edit_user_profile', 'agregar_campos_perfil' );
add_action( 'personal_options_update', 'guardar_campos_registro' );
add_action( 'edit_user_profile_update', 'guardar_campos_registro' );
 
function agregar_campos_perfil( $user ) {
	$user_edad = esc_attr( get_the_author_meta( 'user_edad', $user->ID ) );
	$user_sexo = esc_attr( get_the_author_meta( 'user_sexo', $user->ID ) );
?>
	<h3>Campos adicionales</h3>
	 
	<table class="form-table">
		<tr>
			<th><label for="ciudad">Sexo</label></th>
			<td>
				<input type="radio" name="user_sexo" class="radio" value="h" <?php if($user_sexo=='h') { echo ' checked'; } ?> /> Hombre
				<input type="radio" name="user_sexo" class="radio" value="m" style="margin-left:10px" <?php if($user_sexo=='m') { echo ' checked'; } ?> /> Mujer<br />
			</td>
		</tr>
	</table>
<?php }

 ?>