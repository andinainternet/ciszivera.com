
<?php /**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: CISZI Landing 3
 *
 * @package ciszi-theme
 */
get_header(); ?>
<div id="primary" class="conten-area">
  <main id="main" role="main" class="site-main">
    <div style="margin-top: 1%" class="landing-page-ciszi-banner"><img src="<?php the_field(landing_page_banner) ?>" alt="Eventos"/></div>
    <div class="landing-page-main landing-page-3">
      <div class="landing-page-section2 landing-page-row row">
        <div class="landing-page-col col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="landing-page-col-excerpt">
            <?php echo do_shortcode(the_field(landing_page_3_ciszi_text_1)); ?>
            <div class="l3-btn-registro">
              <p style="text-align: center;"><a style="text-align: center;" href="#registro" class="l3-btn-registro">REGISTRATE				</a></p>
            </div>
          </div>
        </div>
        <div class="landing-page-col col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="landing-page-col-image"><img src="<?php the_field(landing_page_3_img_1); ?>"/></div>
        </div>
      </div>
      <div style="background-color: <?php the_field(landing_page_3_bg); ?>" class="landing-page-section3 landing-page-row row">
        <div class="landing-page-col l3-s2-img col-lg-5 col-md-5 hidden-sm hidden-xs">
          <div class="landing-page-col-image"><img src="<?php the_field(landing_page_3_img_2); ?>"/></div>
        </div>
        <div class="landing-page-col l3-sec-2-video-main col-lg-7 col-md-7 col-sm-7 col-xs-12">
          <div class="landing-page-col-video">
            <?php the_field(landing_page_3_video); ?>
            <div class="l3-btn-registro">
              <p style="text-align: center;"><a style="text-align: center;" href="#registro" class="l3-btn-registro">REGISTRATE</a></p>
            </div>
          </div>
        </div>
      </div>
      <div id="registro" class="landing-page-ciszi-full-form row">
        <div class="landing-page-col col-lg-6 col-md-6 col-sm-6 col-xs-12 l3-form-imgpack">
          <div class="landing-page-item landing-page-image-form"><img src="<?php the_field(full_form_img); ?>" alt=""/></div>
        </div>
        <div class="landing-page-col col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="landing-page-item">
            <h3>Registrate ahora, es gratis</h3>
            <p>Deja tu email para conseguir mas información</p>
            <div class="l3-form-main">
              <?php echo do_shortcode(the_field(full_form_form)); ?>
            </div>
          </div>
          <div class="landing-page-item-social-media">
            <h2>Compartelo con quienes quieras</h2>
            <?php echo do_shortcode('[wpusb]'); ?>
          </div>
        </div>
      </div>
    </div>
  </main>
</div>
<?php get_footer(); ?>