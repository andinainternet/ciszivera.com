jQuery(function(){
	
	//Abrir popup
	jQuery('body').on('click','.ciszi-admin-move',function(event) {
		event.preventDefault();
		jQuery(this).next().slideToggle('fast');
	});

	/*	Abrir crear carpeta	*/
	jQuery('body').on('click','.ciszi-lnk-create-new',function(event) {
		event.preventDefault();
		jQuery(this).next().slideToggle('fast');
	});


	// Removiendo varios posts
	jQuery('body').on('click','.ciszi-admin-remove',function(event) {
		event.preventDefault();
		var datapost = Array();
		var i = 0;

		console.log('click removiendo varios post');

		folder = jQuery('ul.ciszi-list-admin').find('li.active a').attr('data-folder');

		jQuery('input[type=checkbox]:checked').each(function(index,element) {
			datapost[i] = jQuery(this).val();
			i++;
		});

		if(datapost.length == 0) return;

		jQuery('.ciszi-content-admin').html('<br /><br /><br /><div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"><span class="sr-only">100% Complete</span></div></div>');

		jQuery.post(
			CisziAjax.url,
			{
				action: 'ciszi_removing',
				datapost : datapost,
				folder : folder,
				fromto: 'admin',
				wpnonce: CisziAjax.wpnonce
			},
			function(response) {		
				jQuery('.ciszi-content-admin').replaceWith(response);
		});
	});
	

	// Creando una carpeta  vacía 
	jQuery('body').on('click','input[name=ciszi-btn-create-new]',function(event) {
		event.preventDefault();

		var name_folder = jQuery('input[name=ciszi-in-create-new]').val();

		jQuery(this).val('Creando...');
		jQuery(this).attr('disabled','disabled');

		jQuery.post(
			CisziAjax.url,
			{
				action: 'ciszi_create_new',
				folder: name_folder,
				fromto: 'admin',
				wpnonce: CisziAjax.wpnonce
			},
			function(response) {

				var data = jQuery.parseJSON(response);
				//jQuery('.ciszi-win-create-new').slideToggle('fast');
				//jQuery('.ciszi-sidebar-admin').replaceWith(response);

				jQuery('ul.ciszi-list-admin').append(data.sidebar);
				jQuery('ul.ciszi-list-folders').append(data.popup);

				jQuery('input[name=ciszi-in-create-new]').val("");
				jQuery(this).val('Crear Carpeta');
				jQuery(this).removeAttr('disabled');
				jQuery('.ciszi-win-create-new').slideToggle('fast');
		});
	});


	// Click en los checkbox
	jQuery('body').on('click','.ciszi-in-check',function() {

		console.log('click checkbox');

		if( jQuery(this).find('.ciszi-check-icon').length ) {

			jQuery(this).prev().removeAttr('checked');
			jQuery(this).find('.ciszi-check-icon').remove();
			
		} else {

			jQuery(this).prev().attr('checked','checked');
			jQuery(this).html('<span class="ciszi-check-icon glyphicon glyphicon-ok"></span>');
		}


		var sclass = jQuery(this).attr('data-class');
		var objcheck = jQuery('.' + sclass + ' input[type=checkbox]:checked');

		console.log(sclass);
		console.log(objcheck.length);

		if( objcheck.length == 0 ) {
			switch(sclass) {
				case 'ciszi-list-folders' : jQuery('.ciszi-admin-assign').attr('disabled','disabled'); break;
				case 'ciszi-posts' : jQuery('.ciszi-admin-move, .ciszi-admin-remove').attr('disabled','disabled'); break;
				case 'ciszi-custom-admin' : jQuery('.ciszi-admin-fremove').attr('disabled','disabled'); break;
			}
		}
		else {
			switch(sclass) {
				case 'ciszi-list-folders' : jQuery('.ciszi-admin-assign').removeAttr('disabled'); break;
				case 'ciszi-posts' : jQuery('.ciszi-admin-move, .ciszi-admin-remove').removeAttr('disabled'); break;
				case 'ciszi-custom-admin' : jQuery('.ciszi-admin-fremove').removeAttr('disabled'); break;
			}
		}
	});


	//Mover post a carpetas seleccionadas
	jQuery('body').on('click','.ciszi-admin-assign',function(event) {
		event.preventDefault();

		console.log('click mover');
		
		jQuery(this).val('Moviendo...');
		jQuery(this).attr('disabled','disabled');

		var i;
		var array_posts = Array();
		var array_folders = Array();

		i=0;
		jQuery('.ciszi-content-admin input[type=checkbox]:checked').each(function(index,element) {
			array_posts[i] = jQuery(this).val();
			i++;
		});

		i=0;
		jQuery('.ciszi-list-folders input[type=checkbox]:checked').each(function(index,element) {
			array_folders[i] = jQuery(this).val();
			i++;
		});


		jQuery('ul.ciszi-list-admin li.active').removeClass('active');
		jQuery('ul.ciszi-list-admin li:first').addClass('active');
		jQuery('.ciszi-content-admin').html('<br /><br /><br /><div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"><span class="sr-only">100% Complete</span></div></div>');


		jQuery.post(
			CisziAjax.url,
			{
				action: 'ciszi_move_posts',
				dataposts : array_posts,
				datafolders: array_folders,
				wpnonce: CisziAjax.wpnonce
			},
			function(response) {
				jQuery('.ciszi-popup').slideToggle('fast');
				jQuery('.ciszi-content-admin').replaceWith(response);				
		});
	});


	//Seleccionar una carpeta
	jQuery('body').on('click','.ciszi-to-folder', function(event) {
		event.preventDefault();

		jQuery('ul.ciszi-list-admin li.active').removeClass('active');
		jQuery(this).parent().addClass('active');

		jQuery('.ciszi-content-admin').html('<br /><br /><br /><div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"><span class="sr-only">100% Complete</span></div></div>');

		var folder = jQuery(this).attr('data-folder');

		jQuery.post(
			CisziAjax.url,
			{
				action: 'ciszi_get_list',
				folder: folder,
				wpnonce: CisziAjax.wpnonce
			},
			function(response) {

				jQuery('.ciszi-content-admin').replaceWith(response);
		});

	});


	jQuery('body').on('click','.ciszi-admin-folders',function(event) {

		jQuery('.ciszi-content-admin').addClass('disabledcontent');
		jQuery('.ciszi-sidebar-admin').addClass('disabledcontent');
		
		jQuery(this).val('Cargando...');

		jQuery.post(
			CisziAjax.url,
			{
				action: 'ciszi_admin_folders',
				wpnonce: CisziAjax.wpnonce
			},
			function(response) {

				jQuery('.ciszi-sidebar-admin').replaceWith(response);
		});

	});


	jQuery('body').on('click','.ciszi-admin-sfolder', function(event) {

		var i;
		var array_tfolders = Array();
		var array_hfolders = Array();

		jQuery(this).val('Guardando...');

		i=0;
		jQuery('.ciszi-sidebar-admin input[type="text"]').each(function(index,element) {
			array_tfolders[i] = jQuery(this).val();
			i++;
		});

		i=0;
		jQuery('.ciszi-sidebar-admin input[type="hidden"]').each(function(index,element) {
			array_hfolders[i] = jQuery(this).val();
			i++;
		});

		jQuery.post(
			CisziAjax.url,
			{
				action: 'ciszi_admin_folders',
				vfolders: array_tfolders,
				kfolders: array_hfolders,
				wpnonce: CisziAjax.wpnonce
			},
			function(response) {

				jQuery('.ciszi-content-admin').removeClass('disabledcontent');
				jQuery('.ciszi-sidebar-admin').replaceWith(response);
		});
	});


	jQuery('body').on('click','.ciszi-admin-fremove', function(event) {
		
		console.log('Click en borrar');

		var array_folders = Array();
		var i=0;

		jQuery('ul.ciszi-custom-admin input[type="checkbox"]:checked').each(function(index,element) {
			array_folders[i] = jQuery(this).val();
			i++;
		});

		if( array_folders.length == 0) return;

		jQuery(this).val('Borrando...');

		jQuery.post(
			CisziAjax.url,
			{
				action: 'ciszi_admin_fremove',
				folders: array_folders,
				wpnonce: CisziAjax.wpnonce
			},
			function(response) {

				jQuery('.ciszi-sidebar-admin').replaceWith(response);
		});
	});

});