<div id="ciszi-id-<?php echo $postid; ?>" class="ciszi-to-library">
	<a href="" class="ciszi-collection ciszi-saveto" data-postid="<?php echo $postid; ?>"><span class="glyphicon glyphicon-plus-sign"></span> Guardar en tu libreria</a>

	<div class="ciszi-popup" style="display:none;">
		<?php if( $metafolders ) : ?>

			<div class="cizsi-h4">Carpetas</div>

			<ul class="ciszi-list-folders" data-postid="<?php echo $postid; ?>">
			<?php foreach( $metafolders['list_folders'] as $kfolder => $nfolder ) : ?>
				<li>
					<span id="<?php echo $kfolder; ?>" class="" ><?php echo $nfolder; ?></span>

					<span class="ciszi-list-check">
						<input type="checkbox" name="checkfold[]" value="<?php echo $kfolder; ?>" />
						<label class="ciszi-in-check" data-class="ciszi-list-folders"></label>
					</span>
				</li>
			<?php endforeach; ?>
			</ul>
			<input type="submit" name="ciszi-btn-assign-post" value="Guardar" class="btn btn-primary ciszi-front-assign" data-postid="<?php echo $postid; ?>" DISABLED />
		<?php else: ?>
			<p>No hay carpetas creadas</p>
		<?php endif; ?>

		<hr />

		<a href="" class="ciszi-lnk-create-new btn btn-primary">Crear Folder</a>
		<div class="ciszi-win-create-new" style="display:none;">
			<input type="text" name="ciszi-in-create-new" class="ciszi-in-create-new" placeholder="Nombre de la carpeta" /><br />
			<input type="submit" name="ciszi-btn-create-new" class="btn btn-primary" data-postid="<?php echo $postid; ?>" value="Crear" />
		</div>
	</div>
</div>