<div class="pull-right btnGroup-saveto">
	<input type="submit" class="btn btn-primary ciszi-admin-move" data-action="ciszi-admin-move" value="Mover" DISABLED />
	
		<div class="ciszi-popup" style="display:none; right:initial;">
		<?php if( $metafolders ) : ?>

			<div class="cizsi-h4">Carpetas</div>

			<ul class="ciszi-list-folders">
			<?php foreach( $metafolders['list_folders'] as $kfolder => $nfolder ) : ?>
				<li>
					<span id="<?php echo $kfolder; ?>" class="" ><?php echo $nfolder; ?></span>

					<span class="ciszi-list-check">
						<input type="checkbox" name="checkfold[]" value="<?php echo $kfolder; ?>" />
						<label class="ciszi-in-check" data-class="ciszi-list-folders"></label>
					</span>
				</li>
			<?php endforeach; ?>
			</ul>
			<input type="submit" name="ciszi-btn-assign-post" value="Organizar" class="btn btn-primary ciszi-admin-assign" DISABLED />
		<?php else: ?>
			<p>No hay carpetas creadas</p>
		<?php endif; ?>
		</div>
	
	<input type="submit" class="btn btn-primary ciszi-admin-remove" value="Remover" data-action="ciszi-admin-remove" DISABLED />
</div>