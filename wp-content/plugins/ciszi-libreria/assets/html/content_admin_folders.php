<div class="ciszi-content-admin">
	<?php if ( $the_query->have_posts() ) : ?>

		<?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
			<div id="post-<?php the_ID(); ?>" class="ciszi-posts row">
				
				<div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
					<span class="ciszi-list-check">
						<input type="checkbox" name="checkpost[]" value="<?php the_ID(); ?>" />
						<label class="ciszi-in-check" data-class="ciszi-posts"></label>
					</span>

				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
					<div class="ciszi-thumb">
						<?php if ( has_post_thumbnail() ) : ?>
    						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        						<?php the_post_thumbnail('thumbnail',array( 'class' => 'img-responsive' )); ?>
    						</a>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					<h2><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					<div class="ciszi-desc"><?php the_excerpt(); ?></div>
					<div class="ciszi-cat"><?php the_category( ', ' ); ?></div>
				</div>
			</div>
			<hr />
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
	<?php else: ?>
		<h2>No hay información disponible</h2>
	<?php endif; ?>
</div>